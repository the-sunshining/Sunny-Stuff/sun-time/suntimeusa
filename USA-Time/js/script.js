$(document).ready(function() {


    function updateTime() {
        let date = new Date();
        let options = { timeZone: "America/Los_Angeles", hour: "numeric", minute: "numeric", second: "numeric" };
        let PDT = date.toLocaleTimeString("en-US", options);
        options = { timeZone: "America/Denver", hour: "numeric", minute: "numeric", second: "numeric" };
        let MDT = date.toLocaleTimeString("en-US", options);
        options = { timeZone: "America/Chicago", hour: "numeric", minute: "numeric", second: "numeric" };
        let CDT = date.toLocaleTimeString("en-US", options);
        options = { timeZone: "America/New_York", hour: "numeric", minute: "numeric", second: "numeric" };
        let EDT = date.toLocaleTimeString("en-US", options);
        document.getElementById("PDT").innerHTML = PDT;
        document.getElementById("MDT").innerHTML = MDT;
        document.getElementById("CDT").innerHTML = CDT;
        document.getElementById("EDT").innerHTML = EDT;
      }
    
      updateTime();
      setInterval(updateTime, 1000);



});